# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment
PATH="$HOME/.local/bin:$HOME/bin:$PATH"
export PATH

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
alias setclip="xclip -selection c"
alias getclip="xclip -selection c -o"

export LANG=en_US.UTF-8
VISUAL=nvim; export VISUAL; EDITOR="$VISUAL"; export EDITOR
