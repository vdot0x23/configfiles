set tabstop=4
set shiftwidth=4
set softtabstop=4
set noexpandtab

syntax enable

set number relativenumber

set nocompatible              " be iMproved, required
filetype off            " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

Plugin 'Shougo/deoplete.nvim'

call vundle#end()
filetype plugin indent on

inoremap <expr> <Tab> pumvisible() ? "<C-n>" : "<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "<C-p>" : "<S-Tab>"

let g:deoplete#enable_at_startup = 1
